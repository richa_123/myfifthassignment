const CountryList=document.getElementById('CountryList');
const searchbar=document.getElementById('searchbar');
let Country = [];
//console.log(searchbar);

searchbar.addEventListener('keyup',(e) =>{
    const SearchString=e.target.value.toLowerCase();
    //console.log(SearchString);
   // displayCountry(SearchString)
   // console.log(Country,'sta')
    const filteredCharacters = Country.filter((character) => {
       return (
        character.name.common.toLowerCase().includes(SearchString)
       // console.log(character)
       );
    });
    displayCountry(filteredCharacters);
});
const loadCharacters= async () => {
    try{
        const res = await fetch('https://restcountries.com/v3.1/all');
        Country = await res.json();
        displayCountry(Country);
        console.log(Country)
    }
    catch(err){
        console.error(err);
    }
    
};

const displayCountry = (characters) =>{
  console.log(characters)
    const HtmlString = characters
    .map((character) => {
        	if(character.currencies != undefined){
				for(key in character.currencies){
        			currency = character.currencies[key].name;
				}
		}
		if(character.timezones != undefined){
			timezone = character.timezones[0].substring(4).split(':');
			timezone = parseInt(timezone[0])*60+parseInt(timezone[1]);
			timezone = character.timezones[0].charAt(3)+timezone;
		}
        return `
        <div class="card mb-3 m-auto shadow w-50">
                <div class="row">
                  <div class="col-md-4">
              <img src="${character.flags.png}" class="img-fluid mx-4 mt-4" alt="...">
            </div>
            <div class="col-md-8">
            <div class="card-body mx-3 mt-3">
              <h5 class="card-title">${character.name.common}</h5>
              <p class="card-text">Currency:  ${currency}</p>
              <p class="card-text" id="demo"><small class="text-muted">Current date and time:${time('',timezone)}</small></p>
          <a href="${character.maps.googleMaps}" target="_blank"class="btn btn-primary">Show Map</a>
          <a href="details.html?country=${character.name.common}" class="btn btn-primary">Details</a>
            </div>
          </div>
        </div>
      </div>
        `;
    })
    .join('');
    CountryList.innerHTML =HtmlString;

    }
    loadCharacters();
   


    
    
    

